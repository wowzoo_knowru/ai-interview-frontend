import environments from "@/environments";

const getters = {
  isDev: () => environments.name === "dev",
  isProd: () => environments.name === "prod"
};
export default getters;
