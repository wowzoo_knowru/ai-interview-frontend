import { Auth } from "aws-amplify";
import { RestApi } from "@/utils/rest-api";
// import Vue from "vue";

/**
 * TODO: Mutation을 사용하지 않는 Action은 없애야한다.
 * Action은 Mutation을 통해 State를 변화시켜야함.
 * HandleError 또한 RestApi에서 처리해야함.
 */

// const setCandidateFullName = (
//   value,
//   firstName = "first_name",
//   lastName = "last_name",
//   fullName = "full_name"
// ) => {
//   value[fullName] = `${value[firstName]} ${value[lastName]}`;
//   return value;
// };
//
// const setSessionCandidateFullName = value =>
//   setCandidateFullName(
//     value,
//     "candidate_first_name",
//     "candidate_last_name",
//     "candidate_full_name"
//   );

const actions = {
  /*** video ***/
  async uploadVideo({ commit }, payload) {
    const { file, fileName } = payload;
    // console.log(payload);

    try {
      // const toast = Vue.prototype.$toast.info("Uploading video..", {
      //   timeout: 0
      // });
      // const toast = this.$toast.info("Uploading video..", {
      //   timeout: 0
      // });

      const res = await RestApi.upload(fileName, file, {
        contentType: "video/webm;codecs=vp9",
        level: "public"
      });

      commit("UPLOAD_VIDEO", {
        file: res,
        fileName: fileName
      });

      // toast.close();

      return res;
    } catch (e) {
      throw e;
    }
  },
  /*** question ***/
  async getQuestions({ commit }) {
    try {
      const response = await RestApi.getQuestions();
      commit("GET_QUESTIONS", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async addQuestion({ commit }, payload) {
    try {
      const response = await RestApi.addQuestion(payload);
      commit("ADD_QUESTION", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async editQuestion({ commit }, payload) {
    try {
      const response = await RestApi.editQuestion(payload);

      commit("EDIT_QUESTION", {
        name: payload.name,
        question: response
      });
      return response;
    } catch (e) {
      throw e;
    }
  },
  async deleteQuestion({ commit }, payload) {
    try {
      const response = await RestApi.deleteQuestion(payload);
      commit("DELETE_QUESTION", payload.name);
      return response;
    } catch (e) {
      throw e;
    }
  },
  /*** interview ***/
  async getInterviews({ commit }) {
    try {
      const response = await RestApi.getInterviews();
      commit("GET_INTERVIEWS", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async getInterview({ commit }, name) {
    try {
      const response = await RestApi.getInterview(name);
      commit("GET_INTERVIEW", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async addInterview({ commit }, payload) {
    try {
      const response = await RestApi.addInterview(payload);
      commit("ADD_INTERVIEW", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async editInterview({ commit }, payload) {
    try {
      const response = await RestApi.editInterview(payload);
      commit("EDIT_INTERVIEW", {
        name: payload.name,
        interview: response
      });
      return response;
    } catch (e) {
      throw e;
    }
  },
  async deleteInterview({ commit }, payload) {
    try {
      const response = await RestApi.deleteInterview(payload);
      commit("DELETE_INTERVIEW", payload.name);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async sendInvitations({ commit }, { interview, candidates }) {
    try {
      const response = await RestApi.inviteInterview({
        name: interview,
        candidates: candidates
      });
      commit("SEND_INVITATIONS", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  /*** candidate ***/
  async getCandidates({ commit }) {
    try {
      const response = await RestApi.getCandidates();
      commit("GET_CANDIDATES", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async getCandidate({ commit }, name) {
    try {
      const response = await RestApi.getCandidate(name);
      commit("GET_CANDIDATE", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async addCandidate({ commit }, payload) {
    try {
      const response = await RestApi.addCandidate(payload);
      commit("ADD_CANDIDATE", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async editCandidate({ commit }, payload) {
    try {
      const response = await RestApi.editCandidate(payload);
      commit("EDIT_CANDIDATE", {
        name: payload.name,
        candidate: response
      });
      return response;
    } catch (e) {
      throw e;
    }
  },
  async deleteCandidate({ commit }, payload) {
    try {
      const response = await RestApi.deleteCandidate(payload);

      commit("DELETE_CANDIDATE", payload.name);
      return response;
    } catch (e) {
      throw e;
    }
  },
  /*** session ***/
  async getSession({ commit }, name) {
    try {
      const response = await RestApi.getSession(name);
      await commit("GET_SESSION", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async getOpenedSession({ commit }, name) {
    try {
      const response = await RestApi.getCandidateSession(name);
      await commit("GET_SESSION", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async getSessions({ commit }) {
    try {
      const response = await RestApi.getSessions();
      commit("GET_SESSIONS", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async recommendSession({ commit }, payload) {
    try {
      const response = await RestApi.recommendSession(payload);
      commit("RECOMMEND_SESSION", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async decideSession({ commit }, payload) {
    try {
      const response = await RestApi.decideSession(payload);
      commit("DECIDE_SESSION", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async submitAnswer({ commit }, payload) {
    try {
      const response = await RestApi.submitAnswer(payload);
      commit("SUBMIT_ANSWER", payload);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async rateAnswer({ commit }, payload) {
    try {
      const response = await RestApi.rateAnswer(payload);
      commit("RATE_ANSWER", {
        ...response,
        answer: payload
      });
      return response;
    } catch (e) {
      throw e;
    }
  },
  /*** authentication ***/
  async signIn({ commit }, payload) {
    try {
      await RestApi.signIn(payload);
      const user = await Auth.currentAuthenticatedUser();
      commit("SET_USER", user);
      return user;
    } catch (e) {
      throw e;
    }
  },
  async signOut({ commit }) {
    try {
      const response = Auth.signOut();
      commit("SET_USER", null);
      return response;
    } catch (e) {
      throw e;
    }
  },
  /*** recruiter ***/
  async getRecruiters({ commit }) {
    try {
      const response = await RestApi.getRecruiters();
      commit("GET_RECRUITERS", response);
      return response;
    } catch (e) {
      throw e;
    }
  },
  async changePassword({ commit }, payload) {
    try {
      const user = await Auth.currentAuthenticatedUser();
      await RestApi.changePasswordSubmit(user, payload);

      await RestApi.verifyEmail();
      user.attributes.email_verified = true;
      commit("SET_USER", user);
    } catch (e) {
      throw e;
    }
  }
};

export default actions;
