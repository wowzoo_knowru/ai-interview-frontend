// import Vue from "vue";

const removeItemByName = (items, name) => {
  const index = items.findIndex(item => item.name === name);
  if (index > -1) items.splice(index, 1);
};

const mergeItemByName = (items, name, newItem) => {
  const item = items.find(item => item.name === name);
  for (const key of Object.keys(newItem)) {
    item[key] = newItem[key];
  }
};

const mutations = {
  // Video
  UPLOAD_VIDEO(state, { file, fileName }) {
    state.uploaded[fileName] = file;
  },
  // Question
  GET_QUESTIONS(state, questions) {
    state.questions = questions;
  },
  ADD_QUESTION(state, question) {
    state.questions.unshift(question);
  },
  EDIT_QUESTION(state, { name, question }) {
    mergeItemByName(state.questions, name, question);
  },
  DELETE_QUESTION(state, questionName) {
    removeItemByName(state.questions, questionName);
  },
  // Interview
  GET_INTERVIEWS(state, interviews) {
    state.interviews = interviews;
  },
  GET_INTERVIEW(state, interview) {
    state.interview = interview;
  },
  ADD_INTERVIEW(state, interview) {
    state.interviews.unshift(interview);
  },
  EDIT_INTERVIEW(state, { name, interview }) {
    mergeItemByName(state.interviews, name, interview);
  },
  DELETE_INTERVIEW(state, interviewName) {
    removeItemByName(state.interviews, interviewName);
  },
  // Candidate
  GET_CANDIDATES(state, candidates) {
    state.candidates = candidates;
  },
  GET_CANDIDATE(state, candidate) {
    state.candidate = candidate;
  },
  ADD_CANDIDATE(state, candidate) {
    state.candidates.unshift(candidate);
  },
  EDIT_CANDIDATE(state, { name, candidate }) {
    mergeItemByName(state.candidates, name, candidate);
  },
  DELETE_CANDIDATE(state, candidateName) {
    removeItemByName(state.candidates, candidateName);
  },
  // SESSION AND ANSWER
  GET_SESSION(state, session) {
    state.session = session;
  },
  GET_SESSIONS(state, sessions) {
    state.sessions = sessions;
  },
  RECOMMEND_SESSION(state, res) {
    state.session.recommendation = res.recommendation;
    state.sessions.find(s => s.name === res.name).recommend_ratio =
      res.recommend_ratio;
  },
  DECIDE_SESSION(state, res) {
    state.session.decision = res.decision;
    state.sessions.find(s => s.name === res.name).decision = res.decision;
  },
  SUBMIT_ANSWER(state, answer) {
    mergeItemByName(state.session.answers, answer.name, answer);
  },
  RATE_ANSWER(state, { average, answer }) {
    state.session.average = parseFloat(average);
    mergeItemByName(state.session.answers, answer.name, answer);
  },
  SEND_INVITATIONS() {},
  SET_USER(state, user) {
    state.user = user;
  },
  // Recruiter
  GET_RECRUITERS(state, recruiters) {
    state.recruiters = recruiters;
  }
};

export default mutations;
