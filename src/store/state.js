const state = {
  uploaded: {},
  video: null,
  snackbar: null,
  questions: [],
  interviews: [],
  candidates: [],
  recruiters: [],
  sessions: [],
  session: null,
  interview: null,
  candidate: null,
  answer: null,
  user: null,
  totalNumber: {}
};

export default state;
