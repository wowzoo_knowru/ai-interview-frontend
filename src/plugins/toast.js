import Vue from "vue";
import Toast from "@/components/ui/Toast";

const ToastPlugin = {
  install(Vue, options) {
    let cmp = null;

    function createCmp(options) {
      cmp = new Vue(Toast);
      Object.assign(cmp, Vue.prototype.$toast.options, options);
      document
        .getElementsByClassName("application")[0]
        .appendChild(cmp.$mount().$el);

      return cmp;
    }

    function show(message, options = {}) {
      if (cmp) {
        cmp.close();
        Vue.nextTick(() => {
          cmp = null;
          show(message, options);
        });
        return;
      }

      options.message = message;
      return createCmp(options);
    }

    function shorts(options) {
      const colors = ["success", "info", "error", "warning"];
      let methods = {};

      colors.forEach(color => {
        methods[color] = (message, options) =>
          show(message, { color, ...options });
      });
      if (options.shorts) {
        for (let key in options.shorts) {
          let localOptions = options.shorts[key];
          methods[key] = (message, options) =>
            show(message, { ...localOptions, ...options });
        }
      }

      return methods;
    }

    Vue.prototype.$toast = Object.assign(show, {
      options,
      ...shorts(options)
    });
  }
};

Vue.use(ToastPlugin, {
  bottom: true
});
