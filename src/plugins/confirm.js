import Vue from "vue";
import Confirm from "@/components/modal/Confirm";

const ConfirmPlugin = {
  install(Vue, options) {
    function createDialogCmp(options) {
      return new Promise(resolve => {
        const app = document.getElementsByClassName("application")[0];

        const cmp = new Vue(
          Object.assign(Confirm, {
            destroyed: c => {
              app.removeChild(cmp.$el);
              resolve(cmp.value);
            }
          })
        );
        Object.assign(cmp, Vue.prototype.$confirm.options || {}, options);
        app.appendChild(cmp.$mount().$el);
      });
    }

    function show(message, options = {}) {
      options.message = message;
      return createDialogCmp(options);
    }

    Vue.prototype.$confirm = show;
    Vue.prototype.$confirm.options = options || {};
  }
};

Vue.use(ConfirmPlugin);
