import Vue from "vue";
import { RestApi } from "@/utils/rest-api";

import "./vuetify";
import "./confirm";
import "./toast";

const IndexPlugin = {
  install(Vue) {
    Vue.prototype.$rest = RestApi;
  }
};

Vue.use(IndexPlugin);
