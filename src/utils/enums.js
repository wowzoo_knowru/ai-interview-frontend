export const questionTypeEnums = {
  Essay: "short_essay",
  short_essay: "Essay",
  Coding: "coding",
  coding: "Coding",
  Video: "video",
  video: "Video"
};

export const sessionStatusEnums = {
  Completed: "completed",
  completed: "Completed",
  "Not Yet Started": "not_yet_started",
  not_yet_started: "Not Yet Started",
  expired: "Expired",
  Expired: "expired",
  Started: "started",
  started: "Started"
};
