/**
 * Return First Element Of Array
 * @returns {*}
 */
Array.prototype.first = function() {
  return this[0];
};

/**
 * Return Last Element Of Array
 * @returns {*}
 */
Array.prototype.last = function() {
  return this[this.length - 1];
};
