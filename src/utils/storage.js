const suffix = "AI-INTERVIEW";

export class BrowserStorage {
  constructor(storage, suffix) {
    this.storage = storage;
    this.suffix = suffix;
  }

  get(key) {
    const item = this.storage.getItem(key + this.suffix);
    return item ? JSON.parse(item) : null;
  }

  set(key, item) {
    if (item !== null && item !== undefined) {
      this.storage.setItem(key + this.suffix, JSON.stringify(item));
    }
  }

  remove(key) {
    this.storage.removeItem(key + this.suffix);
  }
}

export const LocalStorage = new BrowserStorage(window.localStorage, suffix);
export const SessionStorage = new BrowserStorage(window.sessionStorage, suffix);
