import i18n from "@/utils/i18n";

export default class ValidationRule {
  constructor(label) {
    this.label = i18n.t(label);
    this.validators = [];
  }

  required() {
    this.validators.push(
      v =>
        !!v ||
        i18n.t("validation.required", {
          label: this.label
        })
    );

    return this;
  }

  minLength(compare) {
    this.validators.push(
      v =>
        (v && v.length && v.length >= compare) ||
        i18n.t("validation.minLength", {
          label: this.label,
          compare: compare
        })
    );
    return this;
  }

  maxLength(compare) {
    this.validators.push(
      v =>
        (v && v.length && v.length <= compare) ||
        i18n.t("validation.minLength", {
          label: this.label,
          compare: compare
        })
    );
    return this;
  }

  min(compare) {
    this.validators.push(
      v =>
        v >= compare ||
        i18n.t("validation.minLength", {
          label: this.label,
          compare: compare
        })
    );
    return this;
  }

  max(compare) {
    this.validators.push(
      v =>
        v <= compare ||
        i18n.t("validation.minLength", {
          label: this.label,
          compare: compare
        })
    );
    return this;
  }

  email() {
    this.validators.push(
      v =>
        /\S+@\S+\.\S+/.test(v) ||
        i18n.t("validation.email", {
          label: this.label
        })
    );

    return this;
  }

  password() {
    this.validators.push(
      v =>
        (/[a-z]+/.test(v) && /[0-9]+/.test(v)) ||
        i18n.t("validation.password", {
          label: this.label
        })
    );

    return this;
  }

  value() {
    return this.validators;
  }
}
