import Vue from "vue";
import VueAnalytics from "vue-analytics";
import router from "@/router";
import environments from "@/environments";

const isProd = environments.name === "prod";

Vue.use(VueAnalytics, {
  id: "UA-91412445-10",
  router,
  debug: {
    sendHitTask: isProd
  }
});
