import { Auth, Storage, API } from "aws-amplify";
import environment from "@/environments";
import Vue from "vue";

/**
 * 모든 통신을 모아놓는 곳. 공통 에러처리
 */

const apiName = "AI-Interview-Auth";
const apiUnAuthName = "AI-Interview-UnAuth";

const initData = {
  headers: {}
};

const catchError = err => {
  let message;
  if (err.code && err.code === "UserNotConfirmedException") {
    message = "Please go to your email inbox and check our verification email";
  } else {
    message =
      err.response && err.response.data
        ? err.response.data.Message
        : err.message;
  }
  const error = new Error(message);
  error.stack = err.stack;

  Vue.prototype.$toast.error(message, {
    timeout: 3500
  });

  throw error;
};

export class RestApi {
  static upload(fileName, file, config) {
    return Storage.put(fileName, file, config).catch(catchError);
  }
  static getQuestions() {
    return API.get(apiName, "/question", initData).catch(catchError);
  }
  static addQuestion(value) {
    return API.post(apiName, "/question", {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static editQuestion({ name, ...value }) {
    return API.put(apiName, `/question/${name}`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static deleteQuestion({ name, ...value }) {
    return API.del(apiName, `/question/${name}`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static getInterviews() {
    return API.get(apiName, "/interview", initData).catch(catchError);
  }
  static getInterview(name) {
    return API.get(apiName, `/interview/${name}`, initData).catch(catchError);
  }
  static addInterview(value) {
    return API.post(apiName, "/interview", {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static editInterview({ name, ...value }) {
    return API.put(apiName, `/interview/${name}`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static inviteInterview({ name, ...value }) {
    return API.post(apiName, `/interview/${name}/invite`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static deleteInterview({ name, ...value }) {
    return API.del(apiName, `/interview/${name}`, {
      ...initData,
      body: value
    }).catch(e => {
      throw new Error(e.response.data.Message);
    });
  }
  static getCandidates() {
    return API.get(apiName, "/candidate", initData).catch(catchError);
  }
  static getCandidate(name) {
    return API.get(apiName, `/candidate/${name}`, initData).catch(catchError);
  }
  static addCandidate(value) {
    return API.post(apiName, "/candidate", {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static editCandidate({ name, ...value }) {
    return API.put(apiName, `/candidate/${name}`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static deleteCandidate({ name, ...value }) {
    return API.del(apiName, `/candidate/${name}`, {
      body: value
    }).catch(e => {
      throw new Error(e.response.data.Message);
    });
  }
  static getSessions() {
    return API.get(apiName, "/session", initData).catch(catchError);
  }
  static getSession(name) {
    return API.get(apiName, `/session/${name}`, initData).catch(catchError);
  }
  static getCandidateSession(name) {
    return API.get(apiUnAuthName, `/test/${name}`, initData).catch(catchError);
  }
  static recommendSession({ name, ...value }) {
    return API.post(apiName, `/session/${name}/recommend`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static decideSession({ name, ...value }) {
    return API.post(apiName, `/session/${name}/decide`, {
      ...initData,
      body: value
    });
  }
  static submitAnswer({ name, ...value }) {
    return API.post(apiUnAuthName, `/test/${name}`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static rateAnswer({ name, ...value }) {
    return API.post(apiName, `/answer/${name}/rate`, {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static verifyEmail() {
    return API.post(apiName, "/email/verify", {
      ...initData,
      body: {
        value: "true"
      }
    }).catch(catchError);
  }
  static signUpOrganization(value) {
    return API.post(apiUnAuthName, "/signup", {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static sendSms({ message, phone_number }) {
    return API.post(apiName, "/message/sms", {
      ...initData,
      body: {
        phone_number,
        message
      }
    }).catch(catchError);
  }
  static sendEmail({ message, subject, email, link }) {
    link = link || {};
    return API.post(apiName, "/message/email", {
      ...initData,
      body: {
        email,
        subject,
        message,
        link
      }
    }).catch(catchError);
  }
  static runCode({ language, code }) {
    return API.post(
      apiName,
      `/run/${language === "python" ? language + "3" : language}`,
      {
        ...initData,
        body: {
          files: [
            {
              content: code,
              name: `main.${language === "python" ? "py" : "r"}`
            }
          ],
          language: language
        }
      }
    ).catch(catchError);
  }
  static async inviteUser({
    email,
    organization,
    first_name,
    last_name,
    phone_number,
    country_code,
    url
  }) {
    try {
      let part1 = "";
      let part2 = "";
      const numbers = "0123456789";
      const characters = "abcdefghijklmnopqrstuvwxyz";

      for (let i = 0; i < 3; i++) {
        part1 += numbers.charAt(Math.floor(Math.random() * numbers.length));
        part2 += characters.charAt(
          Math.floor(Math.random() * characters.length)
        );
      }

      let array = (part1 + part2).split("");
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }

      const password = array.join("");

      await Auth.signUp({
        username: email,
        password: password,
        attributes: {
          email: email,
          name: organization,
          given_name: first_name,
          family_name: last_name,
          phone_number: phone_number,
          locale: country_code
        }
      });

      const user = await Auth.currentAuthenticatedUser();
      const username =
        user.attributes.given_name + " " + user.attributes.family_name;

      const subject =
        "[AI Interview] Action Required: " +
        username +
        " invited you to join the " +
        organization +
        " team";

      const message =
        `<p style="margin-bottom: 15px">Hi ` +
        first_name +
        `,</p><p style="margin-bottom: 5px">` +
        username +
        " invited you to join the " +
        organization +
        " team.<br>Please Sign In with this password <strong>" +
        password +
        "</strong></p>";

      const link = {
        url: url,
        message: "Sign In"
      };
      const response = await API.post(apiName, "/message/email", {
        ...initData,
        body: {
          email: email,
          subject: subject,
          message: message,
          link: link
        }
      });

      return response;
    } catch (e) {
      throw catchError(e);
    }
  }
  static async signUp({
    email,
    password,
    organization,
    first_name,
    last_name,
    country_code,
    phone_number
  }) {
    try {
      await this.signUpOrganization({
        user_pool_id: environment.amplify.auth.userPoolId,
        organization: organization
      });

      const response = await Auth.signUp({
        username: email,
        password,
        attributes: {
          email,
          name: organization,
          given_name: first_name,
          family_name: last_name,
          phone_number: country_code + phone_number,
          locale: country_code
        }
      });
      return response;
    } catch (e) {
      throw catchError(e);
    }
  }
  static forgotPassword({ email }) {
    return Auth.forgotPassword(email).catch(catchError);
  }
  static forgotPasswordSubmit({ email, code, new_password }) {
    return Auth.forgotPasswordSubmit(email, code, new_password).catch(
      catchError
    );
  }
  static async changePasswordSubmit(user, { old_password, new_password }) {
    try {
      const response = await Auth.changePassword(
        user,
        old_password,
        new_password
      );
      return response;
    } catch (e) {
      throw catchError(e);
    }
  }
  static signIn({ email, password }) {
    return Auth.signIn(email, password).catch(catchError);
  }

  static requestFreeTrial(value) {
    return API.post(apiUnAuthName, "/request/trial", {
      ...initData,
      body: value
    }).catch(catchError);
  }
  static getRecruiters() {
    return API.get(apiName, "/recruiter", initData).catch(catchError);
  }
}
