import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import i18n from "./utils/i18n";
import Amplify from "aws-amplify";
import environment from "./environments";

import "./plugins";
import "./utils/analytics";
import "./utils/filters";
import "./utils/prototype";
import "./styles.scss";
import "prismjs";
import "prismjs/components/prism-python";
import "sortablejs";
import "vuedraggable";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/markdown/markdown";
import "codemirror/mode/python/python";
import "codemirror/mode/r/r";

Amplify.configure({
  Auth: environment.amplify.auth,
  API: environment.amplify.api,
  Storage: environment.amplify.storage
});

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount("#app");
