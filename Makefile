build-dev:
	npm run build

build-prod:
	npm run build:prod

dev: build-dev
	aws --profile singapore s3 sync --acl public-read --sse --delete ./dist/ s3://dev.ai-interview.com

prod: build-prod
	aws --profile singapore s3 sync --acl public-read --sse --delete ./dist/ s3://prod.ai-interview.com

