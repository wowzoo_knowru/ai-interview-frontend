#!/usr/bin/env bash

mode="$1"
if [[ -z ${mode// } ]]
then
   echo 'development build'
   npm run build
   aws --profile singapore s3 sync --acl public-read --sse --delete ./dist/ s3://dev.ai-interview.com
else
   echo 'production build'
   npm run build:prod
   aws --profile singapore s3 sync --acl public-read --sse --delete ./dist/ s3://prod.ai-interview.com
fi
