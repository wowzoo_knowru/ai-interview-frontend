# AI Interview

## Tech Stack

1. Vue
2. Vuetify
3. Vuex
4. 


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

### Compiles and minifies for production
```
npm run build 
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Build & Deploy
```
./deploy.sh // dev.ai-interview.com
./deploy.sh --prod // www.ai-interview.com
```
 